#!/bin/sh

SOURCES="$(find . -name "*.go")"
OUTPUT="$(go fmt ${SOURCES})"

if [ -z $OUTPUT ]; then
	echo "Clean!"
else
	echo "Code not formatted!"
	exit 1
fi
