package main

import "testing"

func TestMessage(t *testing.T) {
	ret_message := message()

	if ret_message != msg_contents {
		t.Error("Expected '" + msg_contents + "', got '" + ret_message + "'")
	}
}
