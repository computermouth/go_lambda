
VERSION := local
VERSION_FILE=version.go

all: src

fmt:
	./fmt_or_fail.sh

get: fmt
	go get github.com/aws/aws-lambda-go/events
	go get github.com/aws/aws-lambda-go/lambda

src: get
	go build
